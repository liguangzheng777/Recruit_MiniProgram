// 请求工具
function Request(requestBody) {
  var _this = this;
  var url = requestBody.url;
  if (!requestBody.type) { //默认Get请求
    requestBody.type = 'GET';
  }
  let token = wx.getStorageSync('token')
  console.log(token)
  wx.request({
    url: 'https://www.hzfygzs.com/' + url,
    data: requestBody.data,
    method: requestBody.type,
    header: {
      //预留token携带
      "Content-Type": "application/json",
      "Authorization": 'Bearer ' + token
    },
    success(res) {
      requestBody.sCallback(res.data);
    }
  })
}
module.exports = {
  //暴露接口
  Request: Request
}