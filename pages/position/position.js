// pages/position/position.js
import req from '../../utils/request.js'
Page({
  data: {
    id:0,
    list:[]
  },
  onLoad: function(options) {
    this.setData({
      id: options.id
    },()=>{
      this.getPosi()
    })
  },
  // 根据id找岗位信息
  getPosi() {
    let data = {
      url: 'position/id',
      data: {
        id: this.data.id
      },
      sCallback: (res) => {
        if (res.meta.status != 200) {
          return wx.showToast({
            title: '获取详细信息失败！',
            icon: 'none',
            duration: 1000
          })
        }
        this.setData({
          list: res.data
        })
      }
    }
    req.Request(data)
  },
  makephone(e){
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.phone
    })
  }
})