// pages/result/result.js
import req from '../../utils/request.js'

Page({
  data: {
    name: '',
    city: ''
  },
  onLoad: function(options) {
    this.setData({
      name: options.name,
      city: options.city
    })
    let data = {
      url: 'like/position',
      data: {
        name: options.name,
        city: options.city
      },
      sCallback: (res) => {
        if (res.meta.status != 200) {
          return wx.showToast({
            title: '获取列表失败！',
            icon: 'none',
            duration: 1000
          })
        }
        this.setData({
          swiperList: res.data
        })
      }
    }
    req.Request(data)
  },
  // 跳转详细页面
  pospage(e) {
    wx.navigateTo({
      url: `/pages/position/position?id=${e.currentTarget.dataset.id}`
    })
  }
})