import req from '../../utils/request.js'
Page({
  data: {
    tabCur: 0,
    search: [{
      id: 0,
      title: '寻找岗位'
    }, {
      id: 1,
      title: '名企招聘'
    }, {
      id: 2,
      title: '加入人才库'
    }],
    hotList: [],
    comList: [],
    pageNum: 1,
    maxNum: 1
  },
  onLoad() {
    this.getcom()
  },
  //切换 tag
  // tabSelect(e) {
  //   if (e.currentTarget.dataset.id == 0) {
  //     this.getHot()
  //   }
  //   if (e.currentTarget.dataset.id == 1) {
  //     this.getcom()
  //   }
  //   this.setData({
  //     tabCur: e.currentTarget.dataset.id,
  //     maxNum: 1
  //   })
  // },
  // 前往搜索页
  goSearch() {
    wx.navigateTo({
      url: '/pages/search/search'
    })
  },
  // 前往列表页面
  goList(e) {
    wx.navigateTo({
      url: `/pages/list/list?id=${e.currentTarget.dataset.id}`
    })
  },
  // 跳转详细页面
  pospage(e) {
    wx.navigateTo({
      url: `/pages/position/position?id=${e.currentTarget.dataset.id}`
    })
  },
  // 获取热门岗位
  getHot() {
    let data = {
      url: 'position/all',
      data: {
        pageSize: 10,
        pageNum: 1
      },
      sCallback: (res) => {
        if (res.meta.status != 200) {
          return wx.showToast({
            title: '获取岗位列表失败！',
            icon: 'none',
            duration: 1000
          })
        }
        let newList = this.data.hotList.concat(res.data.list)
        this.setData({
          hotList: newList,
          maxNum: res.data.pageNum
        })
      }
    }
    req.Request(data)
  },
  // 获取名企
  getcom() {
    let data = {
      url: 'company/all',
      data: {
        pageSize: 10,
        pageNum: 1
      },
      sCallback: (res) => {
        if (res.meta.status != 200) {
          return wx.showToast({
            title: '获取名企列表失败！',
            icon: 'none',
            duration: 1000
          })
        }
        let newList = this.data.comList.concat(res.data.list)
        this.setData({
          comList: newList,
          maxNum: res.data.pageNum
        })
      }
    }
    req.Request(data)
  },
  //用户触底事件
  onReachBottom: function() {
    let _this = this
    _this.setData({
      pageNum: _this.data.pageNum + 1
    }, () => {
      if (_this.data.pageNum <= _this.data.maxNum) {
        wx.showLoading({
          title: '加载中',
        })
        if (tabCur == 0) {
          _this.getHot()
        } else {
          _this.getcom()
        }
        wx.hideLoading()
      } else {
        //提示已经加载完所有
        wx.showToast({
          title: '已加载所有',
          icon: 'none',
          duration: 1000
        })
      }
    })
  },
  go() {
    wx.navigateTo({
      url: `/pages/data/data`
    })
  }
})