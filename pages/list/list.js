// pages/list/list.js
import req from '../../utils/request.js'
Page({
  data: {
    list:[]
  },
  onLoad: function(options) {
    this.setData({
      id: options.id
    })
    let data = {
      url: `company/${this.data.id}`,
      sCallback: (res) => {
        if (res.meta.status != 200) {
          return wx.showToast({
            title: '获取列表失败！',
            icon: 'none',
            duration: 1000
          })
        }
        this.setData({
          list: res.data.positionList
        })
      }
    }
    req.Request(data)
  },
  // 跳转详细页面
  pospage(e) {
    wx.navigateTo({
      url: `/pages/position/position?id=${e.currentTarget.dataset.id}`
    })
  }
})