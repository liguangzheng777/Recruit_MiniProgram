// pages/search/search.js
import req from '../../utils/request.js'
Page({
  data: {
    index: 0,
    city: ['全国', '北京市', '上海市', '天津市', '重庆市', '河北省', '山西省', '辽宁省', '吉林省', '黑龙江省', '江苏省', '浙江省', '安徽省', '福建省', '江西省', '山东省', '河南省', '湖北省', '湖南省', '广东省', '海南省', '四川省', '贵州省', '云南省', '陕西省', '甘肃省', '青海省', '台湾省'],
    name: '',
    maxNum: 1,
    hotList: []
  },
  onLoad() {
    this.getHot()
  },
  // 修改省份
  PickerChange(e) {
    console.log(e);
    this.setData({
      index: e.detail.value
    })
  },
  // 跳转 搜索结果页面
  search() {
    wx.navigateTo({
      url: `/pages/result/result?name=${this.data.name}&city=${this.data.city[this.data.index]}`
    })
  },
  // 跳转详细页面
  pospage(e) {
    wx.navigateTo({
      url: `/pages/position/position?id=${e.currentTarget.dataset.id}`
    })
  },
  // 修改name
  changeName(e) {
    this.setData({
      name: e.detail.value
    })
  },
  // 获取热门岗位
  getHot() {
    let data = {
      url: 'position/all',
      data: {
        pageSize: 10,
        pageNum: 1
      },
      sCallback: (res) => {
        if (res.meta.status != 200) {
          return wx.showToast({
            title: '获取岗位列表失败！',
            icon: 'none',
            duration: 1000
          })
        }
        let newList = this.data.hotList.concat(res.data.list)
        this.setData({
          hotList: newList,
          maxNum: res.data.pageNum
        })
      }
    }
    req.Request(data)
  },
  //用户触底事件
  onReachBottom: function() {
    let _this = this
    _this.setData({
      pageNum: _this.data.pageNum + 1
    }, () => {
      if (_this.data.pageNum <= _this.data.maxNum) {
        wx.showLoading({
          title: '加载中',
        })
        _this.getHot()
        wx.hideLoading()
      } else {
        //提示已经加载完所有
        wx.showToast({
          title: '已加载所有',
          icon: 'none',
          duration: 1000
        })
      }
    })
  }
})