import req from '../../utils/request.js'

Page({
  data: {
    cardCur: 0,
    swiperList: [],
    maxNum:1,
    pageNum: 1,
    hotList:[]
  },
  goSearch() {
    wx.navigateTo({
      url: '/pages/search/search'
    })
  },
  onLoad() {
    this.getUrl()
    this.getHot()
  },
  onShareAppMessage(res) {
    return {
      title: '猎蜂信息咨询',
      path: '/pages/index/index'
    }
  },
  // 获取首页轮播图
  getUrl() {
    let data = {
      url: 'index/url',
      sCallback: (res) => {
        if (res.meta.status != 200) {
          return wx.showToast({
            title: '获取轮播图失败！',
            icon: 'none',
            duration: 1000
          })
        }
        this.setData({
          swiperList: res.data
        })
      }
    }
    req.Request(data)
  },
  // 获取热门岗位
  getHot() {
    let data = {
      url: 'position/all',
      data: {
        pageSize: 10,
        pageNum: 1
      },
      sCallback: (res) => {
        if (res.meta.status != 200) {
          return wx.showToast({
            title: '获取岗位列表失败！',
            icon: 'none',
            duration: 1000
          })
        }
        let newList = this.data.hotList.concat(res.data.list)
        this.setData({
          hotList: newList,
          maxNum: res.data.pageNum
        })
      }
    }
    req.Request(data)
  },
  onReachBottom: function () {
    let _this = this
    _this.setData({
      pageNum: _this.data.pageNum + 1
    }, () => {
      if (_this.data.pageNum <= _this.data.maxNum) {
        wx.showLoading({
          title: '加载中',
        })
        _this.getHot()
        wx.hideLoading()
      } else {
        //提示已经加载完所有
        wx.showToast({
          title: '已加载所有',
          icon: 'none',
          duration: 1000
        })
      }
    })
  },
  // 跳转详细页面
  pospage(e){
    wx.navigateTo({
      url: `/pages/position/position?id=${e.currentTarget.dataset.id}`
    })
  }
})