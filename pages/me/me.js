// pages/me/me.js
Page({
  data: {

  },
  toData() {
    wx.navigateTo({
      url: '/pages/data/data'
    })
  },
  toPhone(){
    wx.navigateTo({
      url: '/pages/e/e'
    })
  },
  toNone(){
    wx.showToast({
      title: '暂未开放',
      icon: 'none',
      duration: 1000
    })
  }
})